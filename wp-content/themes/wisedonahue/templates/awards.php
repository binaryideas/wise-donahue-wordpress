<?php
$args = array( 'post_type' => 'award', 'order' => 'DESC');
$loop = new WP_Query( $args );
if(!$loop->have_posts()) {return;}
?>
<section class="awards">
    <div class="container">
        <div class="awards-container">
        <?php
            while ( $loop->have_posts() ) : $loop->the_post();
            echo '<div class="award">';
            echo '<img src="'.image_to_base64(wp_get_attachment_url( get_post_thumbnail_id($post->ID))).'" class="img-reponsive"/>';
            echo '</div>';
        endwhile;
        wp_reset_query();
        ?>
        </div>
    </div>
</section>