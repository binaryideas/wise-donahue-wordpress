<?php

function getAttorneysByCategory($category = '') {
    $args = array( 'post_type' => 'attorney', 'order' => 'DESC');
    $loop = new WP_Query( $args );
    if(!$loop->have_posts()) {return;}
    echo '<div class="attorneys">';
    echo '<h2 class="attorney-title">',ucwords(str_replace('-',' ', $category)),'</h2>';
    echo '<ul class="attorneys-list">';
    while ( $loop->have_posts() ) : $loop->the_post();
        $cats = get_the_category();
        if(!has_category($category)) continue; 
        echo '<li>';
        if(has_post_thumbnail()) {
            // the_post_thumbnail( 'full', array('class' => 'img-responsive') );
            echo '<img src="'.image_to_base64(wp_get_attachment_url( get_post_thumbnail_id($loop->ID))).'" class="img-reponsive"/>';
        } else {
            echo '<img class="img-responsive attorney-image" src="'. image_to_base64(get_template_directory_uri() . '/dist/images/attorney-placeholder.jpg') . '"/>';
        }
        the_title('<span class="name">','</span>');
        echo '<a class="btn btn-default" href="'.get_permalink().'">view profile</a>';
        echo '</li>';
    endwhile;
    wp_reset_query();
    ?>
    </ul>
    </div>
    <?php
}
echo '<div class="container">';
getAttorneysByCategory('partners');
getAttorneysByCategory('associates');
getAttorneysByCategory('of-counsel');
echo '</div>';
