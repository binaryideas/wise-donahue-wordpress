<?php
$args = array( 'post_type' => 'career', 'order' => 'DESC');
$loop = new WP_Query( $args );
$post_length = count($loop->posts);
$counter = 0;
if(!$loop->have_posts()) {return;}
echo '<section class="careers">'.
'<div id="career" class="carousel slide career" data-ride="carousel">'.
'<div class="carousel-inner" role="listbox">';
while ( $loop->have_posts() ) : $loop->the_post() ?>
    <div class="item<?= $counter == 0 ? ' active' : '' ?>">
        <div class="career-image col-2 fl same-height" style="background-image: url(<?= get_the_post_thumbnail_url(); ?>)"></div>
        <div class="content col-2 fl same-height">
        <?php 
            the_title('<span class="title-career">','</span>');
            // the_content();
        ?>
        <!-- Indicators -->
        <!-- <ol class="carousel-indicators"> -->
        <?php #for($x = 0; $x < $post_length; $x++) { ?>
            <!-- <li data-target="#career" data-slide-to="<?php #echo $x ?>" class="<?php #echo $x == 0 ? 'active' : ''; ?>"><button><?= $x + 1 ?></button></li> -->
        <?php #} ?>
        <!-- </ol> -->
        </div>
    </div>
<?php 
    $counter++;
    endwhile;
    wp_reset_query();
?>
</div>

<!-- Left and right controls -->
  <a class="left carousel-control" href="#career" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#career" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  
</div>
</section>