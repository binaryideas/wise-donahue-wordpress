<?php
$args = array( 'post_type' => 'attorney', 'order' => 'DESC');
$loop = new WP_Query( $args );
if(!$loop->have_posts()) {return;}
echo '<ul>';
while ( $loop->have_posts() ) : $loop->the_post();
    the_title('<li><a href="'.get_permalink().'">','</a></li>');
  endwhile;
  wp_reset_query();
?>
</ul>