<?php
$args = array( 'post_type' => 'homepage-slider', 'order' => 'DESC');
$loop = new WP_Query( $args );
if(!$loop->have_posts()) {return;}
$counter = 1;
echo '<div class="slider hero-slider"><ul class="slides">';
while ( $loop->have_posts() ) : $loop->the_post();
$content = get_the_content();
?>
    <li class="item full-bg" style="background-image: url(<?= get_the_post_thumbnail_url(); ?>)">
    	<?php if($content != '') { ?>
	    	<div class="hero-slider__content-container">
	    		<div class="hero-slider__content">
					<?= get_the_content(); ?>
				</div>
	    	</div>
    	<?php } ?>
    </li>
<?php endwhile;
wp_reset_query();
?>
</ul>
<div class="overlay"></div>
</div>