<?php
  $video = CFS()->get('hero_video');
  $poster = CFS()->get('hero_video_poster');
?>
<header class="banner full-height">
  <!-- <div id="big-video" class="bg-full" style="background-image: url(<?php #echo image_to_base64($poster) ?>)"> -->
  <?php get_template_part('templates/slider'); ?>
  <?php get_template_part('templates/navbar'); ?>
  <div class="hero">
    <div class="hero-container">
      <?php if( !empty($video) ) { ?>
      <a href="javascript:void(0)" class="play hidden-xs"></a>
      <?php } ?>
      <div class="tagline"><?= get_bloginfo('description') ?></div>
      <a 
        class="btn btn-default btn-big"
        data-scroll href="#request-a-free-consultation" 
        data-options='{
          "speed": 2000,
          "easing": "easeInOutCubic"
        }'
        >REQUEST FREE CONSULTATION</a>
      <div class="direction">
        <span class="caption">scroll down</span>
        <img src="<?= get_template_directory_uri() . '/dist/images/arrow-down.png'; ?>" alt="scroll down" class="img-responsive center-block">
      </div>
    </div>
  </div>
</header>