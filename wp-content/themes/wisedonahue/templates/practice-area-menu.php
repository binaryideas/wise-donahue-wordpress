<?php
$args = array( 'post_type' => 'practice-area', 'order' => 'DESC');
$loop = new WP_Query( $args );
$counter = 1;
$current_page = $post->post_name;
if(!$loop->have_posts()) {return;}
?>
<div class="practice-area-menu">
<?php
  while ( $loop->have_posts() ) : $loop->the_post() ?>
    <a class="menu-item<?= $current_page == $post->post_name ? ' active' : '' ?>" href="<?= get_permalink() ?>">
      <span class="icon-container">
      <?php #wp_get_attachment_image(CFS()->get('image'), '', true, array('class' => 'icon')); ?>
      <img src="<?= wp_get_attachment_url(CFS()->get('image')); ?>" class="icon"/>
      </span>
      <div class="text">
      <?php the_title('<span class="menu-title">','</span>'); ?>
      </div>
    </a>
  <?php
  endwhile;
  wp_reset_query();
?>
</div>