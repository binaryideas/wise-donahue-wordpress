<div class="post-action upper">
    <time class="updated" datetime="<?= get_post_time('c', true); ?>">
        <img class="post-action__icon" src="<?= image_to_base64(get_template_directory_uri() .'/dist/images/icons/calendar.png') ?>" alt="date" />
        <?= get_the_date('M j, Y'); ?>
    </time>
    <p class="byline author vcard">
    <img class="post-action__icon" src="<?= image_to_base64(get_template_directory_uri() .'/dist/images/icons/person.png') ?>" alt="date" />
    <a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn">
    <?= get_the_author(); ?></a>
    </p>
    <a href="<?php the_permalink(); ?>/#respond">
    <img class="post-action__icon" src="<?= image_to_base64(get_template_directory_uri() .'/dist/images/icons/comment.png') ?>" alt="date" />
    Leave a comment</a>
    <div class="categories">
    <img class="post-action__icon" src="<?= image_to_base64(get_template_directory_uri() .'/dist/images/icons/archive.png') ?>" alt="date" />
    <?php the_category(', '); ?> 
    </div>
</div>
<div class="post-action lower tags">
    <?php the_tags('<img class="post-action__icon" src="'.image_to_base64(get_template_directory_uri() .'/dist/images/icons/tag.png').'" alt="date" />',', ','') ?>
</div>