<?php
$facebook = get_option('facebook_username',null);
$twitter = get_option('twitter_username',null);
$googleplus = get_option('google_plus_username',null);
$linkedin = get_option('linkedin_username',null);
?>
<section id="request-a-free-consultation">
    <div class="container">
    <?= do_shortcode( '[contact-form-7 id="198" title="Request a free Consultation"]' ); ?>
    <div class="row footer-widget-container">
        <div class="col-sm-5">
            <div class="footer-logo"><img src="<?= get_template_directory_uri() .'/dist/images/logo-white.png'; ?>" alt="Wise and Donahue" class="img-responsive"></div>
            <!--
            <div class="social-media-links">
                <?php if(!is_null($facebook)) { ?>
                <a class="facebook" target="_blank" href="https://www.facebook.com/<?= get_option('facebook_username') ?>">facebook</a>
                <?php } ?>
                <?php if(!is_null($twitter)) { ?>
                <a class="twitter" target="_blank" href="https://twitter.com/<?= get_option('twitter_username') ?>">twitter</a>
                <?php } ?>
                <?php if(!is_null($googleplus)) { ?>
                <a class="googleplus" target="_blank" href="https://plus.google.com/+<?= get_option('google_plus_username') ?>">google+</a>
                <?php } ?>
                <?php if(!is_null($linkedin)) { ?>
                <a class="linkedin" target="_blank" href="https://www.linkedin.com/company/<?= get_option('linkedin_username') ?>">linkedin</a>
                <?php } ?>
            </div>
            -->
            <div class="newsletter">
                <h3 class="newsletter-title white">Receive a Newsletter</h3>
                <?= do_shortcode( '[contact-form-7 id="199" title="Newsletter"]' ) ?>
            </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
            <div class="recent-post footer-widget">
            <h4 class="footer-widget-title">Recent Posts</h4>
            <?php get_template_part('templates/recent-post'); ?>
            </div>
            <div class="attorneys-list-footer footer-widget">
            <h4 class="footer-widget-title">Attorneys</h4>
            <?php get_template_part('templates/attorneys-footer'); ?>
            </div>
        </div>
    </div>
    </div>
</section>