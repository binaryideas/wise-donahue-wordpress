<article <?php post_class(); ?>>
  <header class="post-header">
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/share'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
  <?php get_template_part('templates/entry-meta'); ?>
</article>