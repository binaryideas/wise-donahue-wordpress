<?php use Roots\Sage\Titles; ?>
<?php 
$title;
if(CFS()->get( 'use_custom_title' )) {
	$title = CFS()->get( 'custom_title' );
} else { 
	$title = Titles\title();
} 
?>
<div class="page-header container">
	<h1 class="page-title"><?= $title ?></h1>
</div>
