<div data-scroll-header class="container-fluid nav-container" data-spy="affix" data-offset-top="197">
  <div class="nav-outer">
    <div class="nav-inner">
      <div class="cta-top hidden-xs">
          <span><i class="glyphicon glyphicon-earphone"></i> <a href="tel:4102802023">(410) 280-2023</a></span>
          <span><i class="glyphicon glyphicon-earphone"></i> <a href="tel:7039346377">(703) 934-6377</a></span>
      </div>
      <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-target="#main-menu">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
            <img src="<?= get_template_directory_uri() . '/dist/images/logo.png'; ?>" alt="Wise and Donahue" class="img-responsive">
          </a>
        </div>
        <div id="main-menu">
            <button class="close-nav"><i class="glyphicon glyphicon-remove"></i></button>
            <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-right','container'=>'']);
            endif;
            ?>
        </div>
      </nav>
    </div>
  </div>
</div>