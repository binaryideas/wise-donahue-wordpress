<?php
$addons = CFS()->get('addon_item');
$length = count($addons);
?>
<section class="addons">
<?php
    for($i=0;$i<$length;$i++) {     
?>
    <div class="addon <?= $i % 2 == 0 ? 'even' : 'odd' ?>">
        <div class="addon-image addon-item <?= $i % 2 == 0 ? 'left' : 'right' ?>">
        <div class="addon-image__image" style="background-image: url(<?= $addons[$i]['addon_image']; ?>)"></div>
            <div class="addon-image__caption">
                <?php if (!empty($addons[$i]['addon_image_caption'])) echo $addons[$i]['addon_image_caption']; ?>
            </div>
        <?php if($addons[$i]['addon_image_caption']) { ?>
            <div class="overlay"></div>
        <?php } ?>
        </div>
        <div class="addon-content addon-item <?= $i % 2 == 0 ? 'right' : 'left' ?>"><?= $addons[$i]['addon_content'] ?></div>
    </div>
<?php 
    }
?>
<span class="clearfix"></span>
</section>
