<?php get_template_part('templates/locations'); ?>
<?php get_template_part('templates/request-a-free-consultation'); ?>
<footer class="content-info">
  <div class="container">
    <div class="dtable">
        <div class="developer hidden-xs cell">Designed by <a href="https://binaryideas.com" target="_blank">Binary Ideas Online Marketing</a></div>
        <div class="copyright cell">
        <?php
            // if (has_nav_menu('footer_navigation')) :
            //     wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'footer-links','container'=>'']);
            // endif;
        ?>
        <div class="text">
            &copy;2017 Wise & Donahue, PLC., All Rights Reserved.
        </div>
        <div class="developer visible-xs">Designed by <a href="https://binaryideas.com" target="_blank">Binary Ideas Online Marketing</a></div>
        </div>
    </div>
  </div>
</footer>
<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Gentium+Book+Basic:400,700' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})(); </script>