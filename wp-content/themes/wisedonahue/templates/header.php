<header class="banner sub" <?= CFS()->get('banner') ? 'style="background-image: url('.CFS()->get('banner').')"' : '' ?>>
    <?php get_template_part('templates/navbar'); ?>
    <div class="tagline-container">
        <!--<div class="tagline"><?php #echo get_bloginfo('description') ?></div>-->
        <a 
        	href="#request-a-free-consultation" 
        	class="btn btn-default btn-big"
	        data-scroll href="#request-a-free-consultation" 
	        data-options='{
	          "speed": 2000,
	          "easing": "easeInOutCubic"
	        }'
        	>REQUEST FREE CONSULTATION</a>
    </div>
    <?php #get_template_part('templates/slider'); ?>
</header>