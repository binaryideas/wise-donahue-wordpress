<div class="container">

<?php $fields = CFS()->get( 'file' ); ?>
<?php 
if(count($fields) > 0) {
	echo '<div class="files">';
	foreach($fields as $field) { ?>
	<div class="file same-height">
        <a href="<?= wp_get_attachment_url($field['file_item']) ?>" target="_blank">
            <div class="file__thumnail"><img class="img-responsive center-block" src="<?= get_template_directory_uri() ?>/dist/images/file.png"></div>
            <div class="file__info">
                <span class="file__info--name"><?= get_the_title($field['file_item']) ?></span>
            </div>
        </a>
	</div>
<?php } 
	echo '</div>';
}?>
</div>