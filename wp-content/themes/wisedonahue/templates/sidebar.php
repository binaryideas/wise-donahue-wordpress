<div class="post-sidebar">
    <div class="sidebar-widget">
    <h4 class="sidebar-title">Recent Posts</h4>
    <?php get_template_part('templates/recent-post'); ?>
    </div>
    <div class="sidebar-widget">
    <h4 class="sidebar-title">Archives</h4>
    <ul>
    <?php 
        wp_get_archives(array(
            'limit' => 10
        )); 
    ?>
    </ul>
    </div>
    <div class="sidebar-widget">
    <h4 class="sidebar-title">Categories</h4>
    <ul>
    <?php 
        wp_list_categories(array(
            'exclude' => array(4,5,6),
            'title_li' => ''
        ));
    ?>
    </ul>
    </div>
</div>