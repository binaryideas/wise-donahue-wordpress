<div class="container mt-70">
  <div class="row">
    <div class="col-md-3 hidden-xs hidden-sm">
    <?php get_template_part('templates/sidebar'); ?>
    </div>
    <div class="col-md-9">
    <?php while (have_posts()) : the_post(); ?>
      <article <?php post_class(); ?>>
        <header class="post-header">
          <h1 class="entry-title"><?php the_title(); ?></h1>
          <?php get_template_part('templates/share'); ?>
        </header>
        <div class="entry-content">
          <?php the_content(); ?>
        </div>
        <?php get_template_part('templates/entry-meta'); ?>
        <footer>
          <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
        </footer>
        <?php comments_template('/templates/comments.php'); ?>
      </article>
    <?php endwhile; ?>
    </div>
    <div class="col-md-3 visible-xs visible-sm">
    <?php get_template_part('templates/sidebar'); ?>
    </div>
  </div>
</div>