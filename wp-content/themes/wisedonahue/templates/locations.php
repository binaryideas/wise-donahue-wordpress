<?php
$args = array( 'post_type' => 'location', 'order' => 'DESC');
$loop = new WP_Query( $args );
if(!$loop->have_posts()) {return;}
echo '<span class="clearfix"></span>';
echo '<section class="locations"><div class="container">';
echo '<h2 class="section-title"    >locations</h2>';
echo '<div class="row">';
while ( $loop->have_posts() ) : $loop->the_post();
	$linkTo = CFS()->get('link_page_to');
    echo '<div class="location">';
    // the_post_thumbnail( 'full', array('class' => 'img-responsive') );
    echo '<img src="'.wp_get_attachment_url( get_post_thumbnail_id($loop->ID)).'" class="img-responsive"/>';
    if ($linkTo['url']) {
    	echo '<a href="'.$linkTo['url'].'" target="'.$linkTo['target'].'">';
    }
    the_title('<span class="title-location">','</span>');
    if ($linkTo['url']) {
    	echo '</a>';
    }
    echo '<div class="content">', the_content() ,'</div>';
    echo '</div>';
  endwhile;
  wp_reset_query();
?>
</div>
</div>
</section>