<?php use Roots\Sage\Titles; ?>
<div class="page-header page-blog-header container">
	<div class="row">
		<h1 class="page-title col-md-9 col-md-offset-3"><?= Titles\title() ?></h1>
	</div>
</div>