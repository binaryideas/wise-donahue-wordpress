<div class="container mt-80 pt-70">
<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <div class="caption">
      <?php foreach(get_the_category() as $category) { echo $category->cat_name . ' '; } ?>
      </div>
    </header>
    <div class="entry-content">
      <div class="row first-row single-attorney-row">
        <div class="col-sm-3">
          <?php 
            if(has_post_thumbnail()) {
              // the_post_thumbnail( 'full', array('class'=>'img-responsive') );
              echo '<img src="'.wp_get_attachment_url( get_post_thumbnail_id($post->ID)).'" class="img-responsive attorney-image"/>';
            } else {
              echo '<img class="img-responsive attorney-image" src="'. get_template_directory_uri() . '/dist/images/attorney-placeholder.jpg' . '"/>'; 
            }
          ?>
        </div>
        <div class="col-sm-9 attorney-description">
        <?php
          if(CFS()->get('description')) {
            echo CFS()->get('description');
          } else {
            echo 'No information available.';
          } 
        ?>
        </div>
      </div>

      <div class="row second-row single-attorney-row">
        <div class="col-sm-3">
          <div class="contacts">
          <?php 
            if(!CFS()->get('email') && !CFS()->get('phone') && !CFS()->get('fax')) echo 'Contact information not available.'
          ?>
            <?php if(CFS()->get('email')) { ?>
            <div class="field">
              <span class="field-label">Email</span>
              <span class="field-value"><a href="mailto:<?= CFS()->get('email') ?>"><?= CFS()->get('email') ?></a></span>
            </div>
            <?php } ?>
            <?php if(CFS()->get('phone')) { ?>
            <div class="field">
              <span class="field-label">Phone</span>
              <span class="field-value"><?= CFS()->get('phone') ?></span>
            </div>
            <?php } ?>
            <?php if(CFS()->get('fax')) { ?>
            <div class="field">
              <span class="field-label">Fax</span>
              <span class="field-value"><?= CFS()->get('fax') ?></span>
            </div>
            <?php } ?>
          </div>
        </div>
        <div class="col-sm-9 experiences">
          <?php
            $experiences = CFS()->get('experiences');
            if (sizeOf($experiences)>0) {
              foreach($experiences as $experience) { ?>
              <span class="experience-label"><?= $experience['title']; ?></span>
              <span class="experience-description"><?= nl2br($experience['description']); ?></span>
          <?php } } else {
            echo 'No information available.';
          } ?>
        </div>
      </div>

      <?php if(!empty_content($post->post_content)) { ?>
      <div class="single-attorney-row third-row">
        <?php the_content() ?>
      </div>
      <?php } ?>
    </div>
    <footer class="post-control">
      <?php previous_post_link('<span class="prev-control">&larr; %link</span>'); ?>    <?php next_post_link('<span class="next-control">%link &rarr;</span>'); ?>
    </footer>
  </article>
<?php endwhile; ?>
</div> 