<?php
if (post_password_required()) {
  return;
}
?>

<section id="comments" class="comments">
  <?php if (have_comments()) : ?>
   
    <ol class="comment-list">
      <?php wp_list_comments(['style' => 'ol', 'short_ping' => true, 'avatar_size' => 80, 'callback' => 'custom_comment_list']); ?>
    </ol>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
      <nav>
        <ul class="pager">
          <?php if (get_previous_comments_link()) : ?>
            <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'sage')); ?></li>
          <?php endif; ?>
          <?php if (get_next_comments_link()) : ?>
            <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'sage')); ?></li>
          <?php endif; ?>
        </ul>
      </nav>
    <?php endif; ?>
  <?php endif; // have_comments() ?>

  <?php if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments')) : ?>
    <div class="alert alert-warning">
      <?php _e('Comments are closed.', 'sage'); ?>
    </div>
  <?php endif; ?>

  <?php 
    $fields = array(
      'email' =>
      '<div><div class="form-group comment-form-email"><label class="sr" for="email">' . __( 'Email', 'domainreference' ) . '</label>
      <input id="email" placeholder="Email (required)" name="email" class="form-control" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
      '" size="30" required /></div>',

      'author' => 
      '<div class="form-group comment-form-author"><label for="author"class="sr">' . __( 'Name', 'domainreference' ) . '</label>
      <input id="author" placeholder="Name (required)" name="author" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author'] ) .
      '" size="30" required /></div>',
      'url' =>
      '<div class="form-group comment-form-url"><label class="sr" for="url">' . __( 'Website', 'domainreference' ) . '</label>' .
      '<input id="url" placeholder="Website" name="url" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author_url'] ) .
      '" size="30" /></div>',
    );
    comment_form(array(
      'title_reply' => '<span class="comment-icon custom-icon toggle-comment-form"></span>Leave a comment',
      'comment_notes_before' => '',
      'comment_notes_after' => '',
      'comment_field' => '<div class="form-group comment-form-comment"><label class="sr-only" for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" class="form-control" placeholder="Enter your comment here..." name="comment" cols="45" rows="8" aria-required="true"></textarea></div>',
      'class_submit' => 'btn btn-default',
      'label_submit' => 'Post',
      'fields' => apply_filters('comment_form_default_fields',$fields)
    )); 
  ?>
</section>
