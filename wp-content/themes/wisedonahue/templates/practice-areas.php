<?php
$args = array( 'post_type' => 'practice-area', 'order' => 'DESC');
$loop = new WP_Query( $args );
$counter = 1;
if(!$loop->have_posts()) {return;}
?>
<section class="practice-area-list container">
<?php
  while ( $loop->have_posts() ) : $loop->the_post();
    // if ($counter % 3 == 1) echo '<div class="row">';

    echo '<div class="practice-area-list__item col-xs-6 col-sm-6 col-md-4">';
    echo '<a href="'.get_permalink().'">';
    echo '<img src="'.wp_get_attachment_url(CFS()->get('image')).'" class="img-responsive center-block"/>';
    // echo wp_get_attachment_image(CFS()->get('image'), 'full', true, array('class' => 'img-responsive center-block'));
    the_title('<h1 class="title">','</h1>');
    echo '</a>';
    echo '</div>';

    // if ($counter % 3 == 0) echo '</div>';
    
    $counter++;
  endwhile;
  wp_reset_query();
?>
</section>