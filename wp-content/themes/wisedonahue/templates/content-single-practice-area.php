<div class="container mt-80 pt-70">
<div class="row">
<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class('col-md-8'); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
  </article>
<?php endwhile; ?>
<aside class="sidebar col-md-3 col-sm-offset-1">
  <h2 class="sidebar-title">Practice Areas</h2>
  <?php get_template_part( 'templates/practice-area-menu' ); ?>
</aside>
</div>
</div>
<?php get_template_part('templates/testimonials'); ?>