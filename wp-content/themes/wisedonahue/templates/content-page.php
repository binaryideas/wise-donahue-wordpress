<?php
    $arr = CFS()->get('text_align');
    $text_align = 'text-left';
    if(!empty($arr)) {
        $arr_align = array_keys($arr);
        foreach($arr_align as $value) {
            $text_align = 'text-'.$value;
        }
    } else {
        $text_align = '';
    }
?>
<div class="page-content <?= $text_align ?> <?= !CFS()->get('full_width_container') ? 'container' : '' ?>">
    <?php 
        if(CFS()->get('full_width_container')) {
            echo '<div class="container">';
        }
    ?>
    <?php the_content(); ?>
    <?php 
        if(CFS()->get('full_width_container')) {
            echo '</div>';
        }
    ?>
</div>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
