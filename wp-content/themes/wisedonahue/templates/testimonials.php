<?php
$args = array( 'post_type' => 'testimonial', 'order' => 'DESC');
$loop = new WP_Query( $args );
if(!$loop->have_posts()) {return;} ?>
<section class="testimonials-container">
	<div id="testimonials-carousel" class="testimonials carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
		<?php
		$counter = 0;
		while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="item<?= ! $counter ? ' active' : ''; ?>">
			<div class="container">
				<div class="testimonial">
				    <div class="content"><?php the_content(); ?></div>
				    <?php the_title('<span class="title-testimonial">','</span>'); ?>
			    </div>
		    </div>
		    </div>
	    <?php
	    $counter++;
		endwhile;
		wp_reset_query();
		?>
		</div>
		<a class="left carousel-control" href="#testimonials-carousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#testimonials-carousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</section>