<?php
/**
 * Template Name: News and Article Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/files'); ?>
  <div class="page-content-container brand-primary">
  <?php get_template_part('templates/content', 'page'); ?>
  </div>
<?php endwhile; ?>
