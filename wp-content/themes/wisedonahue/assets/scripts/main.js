/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  
	function isMobile() {
	    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
	}

  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        // $('.addon-item,.same-height').matchHeight();
        $('.navbar-toggle').click(function() {
          var navbar = $(this).attr('data-target');
          $(navbar).toggleClass('slide');
        });
        $('.close-nav').click(function() {
          $(this).parent().removeClass('slide');
        });
        function setAddonHeight() {
          if (window.innerWidth > 767) {
            $('.addon-item,.same-height').matchHeight();
          }
          if (window.innerWidth < 768) {
            $('.addon-item,.same-height').matchHeight({
              byRow: false
            });
          }
        }
        setAddonHeight();
        $(window).resize(function() {
          setAddonHeight();
        });
        
        $('.slider').flexslider({
          animation: "fade",
          directionNav: false,
          controlsContainer: $('.hero')
          // controlNav: false
        });

           $('.carousel').carousel({
		        interval: false
		    }).on('slide.bs.carousel', function (e) {
		        var nextH = $(e.relatedTarget).height();
		        $(this).find('.active.item').parent().animate({
		            height: nextH
		        }, 500);
		    });

        smoothScroll.init({
			selectorHeader: '[data-scroll-header]'
        });

        if (!isMobile()) {
        	$('a[href*=tel],a[href*=fax]').click(function() {
        		return false;
        	}).addClass('normal-cursor');
        }
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
        $('.practice-area-list__item').matchHeight();
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    'careers': {
      init: function() {
        // JavaScript to be fired on the about us page
        $('#career').on('slid.bs.carousel', function (e) {
          var i = $(e.relatedTarget).index();
          $(this).find('[data-slide-to='+i+']').addClass('active');
        });
      }
    },
    'practice_areas': {
      init: function() {
        $('.practice-area-list__item').matchHeight();
      }
    }

   
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
