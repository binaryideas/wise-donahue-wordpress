<?php get_template_part('templates/page', 'blog-header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<div class="container">
  <div class="row">
    <div class="col-md-3 hidden-xs hidden-sm">
    <?php get_template_part('templates/sidebar'); ?>
    </div>
    <div class="col-md-9">
    <?php while (have_posts()) : the_post(); ?>
      <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
    <?php endwhile; ?>
    </div>
    <div class="col-md-3 visible-xs visible-sm">
    <?php get_template_part('templates/sidebar'); ?>
    </div>
  </div>
</div>
<?php the_posts_navigation(); ?>
