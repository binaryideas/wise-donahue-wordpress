<?php get_template_part('templates/page', 'header'); ?>

<div class="container">
  <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
  <br />
</div>