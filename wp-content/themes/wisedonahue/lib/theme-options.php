<?php
// create custom plugin settings menu
add_action('admin_menu', 'theme_option');

function theme_option() {

	//create new top-level menu
	add_menu_page('Theme Settings', 'Theme Settings', 'administrator', __FILE__, 'bi_theme_settings_page' );

	//call register settings function
	add_action( 'admin_init', 'register_bi_theme_settings' );
}


function register_bi_theme_settings() {
	//register our settings
	register_setting( 'bi-theme-settings-settings-group', 'facebook_username' );
	register_setting( 'bi-theme-settings-settings-group', 'twitter_username' );
	register_setting( 'bi-theme-settings-settings-group', 'google_plus_username' );
	register_setting( 'bi-theme-settings-settings-group', 'linkedin_username' );
}

function bi_theme_settings_page() {
?>
<div class="wrap">
<h1>Theme Settings</h1>

<form method="post" action="options.php">
    <?php settings_fields( 'bi-theme-settings-settings-group' ); ?>
    <?php do_settings_sections( 'bi-theme-settings-settings-group' ); ?>
    <h2 class="title">Social Media</h2>
    <p>Add the username of your social media below.</p>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Facebook</th>
        <td><input type="text" name="facebook_username" value="<?php echo esc_attr( get_option('facebook_username') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Twitter</th>
        <td><input type="text" name="twitter_username" value="<?php echo esc_attr( get_option('twitter_username') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Google+</th>
        <td><input type="text" name="google_plus_username" value="<?php echo esc_attr( get_option('google_plus_username') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row">Linkedin</th>
        <td><input type="text" name="linkedin_username" value="<?php echo esc_attr( get_option('linkedin_username') ); ?>" /></td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>