<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/theme-options.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function empty_content($str) {
    return trim(str_replace('&nbsp;','',strip_tags($str))) == '';
}

/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 200;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return ' <a href="'.get_the_permalink().'" class="read-more" rel="nofollow">read more...</a>';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


function custom_comment_list( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' : ?>
            <li <?php comment_class(); ?> id="comment<?php comment_ID(); ?>">
            <div class="back-link">< ?php comment_author_link(); ?></div>
        <?php break;
        default : ?>
            <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
            <article <?php comment_class(); ?> class="comment">
              <div class="comment-body">
                <div class="author vcard">
                  <div class="row">
                    <div class="col-sm-2 author-gravatar">
                      <img src="<?php echo image_to_base64(get_avatar_url($comment, array('size' => 80))) ?>" class="avatar avatar-80 photo" />
                      <?php #echo get_avatar( $comment, 80 ); ?>
                    </div>
                    <div class="col-sm-10">
                      <span class="author-name"><?php comment_author(); ?></span>
                      <div class="comment_text"><?php comment_text(); ?></div>
                      <footer class="comment-footer">
                        <div class="reply"><?php 
                        comment_reply_link( array_merge( $args, array( 
                        'reply_text' => 'Reply',
                        'after' => '', 
                        'depth' => $depth,
                        'max_depth' => $args['max_depth'] 
                        ) ) ); ?>
                        </div><!-- .reply -->
                      </footer><!-- .comment-footer -->
                    </div>
                  </div>
                </div><!-- .vcard -->
              </div><!-- comment-body -->
            </article><!-- #comment-<?php comment_ID(); ?> -->
        <?php // End the default styling of comment
        break;
    endswitch;
}

function image_to_base64($path_to_image)
{ 
    $type = pathinfo($path_to_image, PATHINFO_EXTENSION);
    $image = file_get_contents($path_to_image);
    return 'data:image/' . $type . ';base64,' . base64_encode($image);
}

function md_footer_enqueue_scripts() {
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);
}
add_action('wp_enqueue_scripts', 'md_footer_enqueue_scripts');

add_action( 'wp_print_styles', 'remove_unecessary_css', 100 );
function remove_unecessary_css() {
  wp_deregister_style( 'contact-form-7' ); 
  wp_deregister_style( 'A2A_SHARE_SAVE' );
}