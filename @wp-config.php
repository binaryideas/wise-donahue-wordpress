<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wiseanddonahuewp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1:8889');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'C|J!/{;7}fZP=1[clEVnZG+9P`2uY]2pTSP JO=CUQE1Av4/!p9=WjvtO)6t718u');
define('SECURE_AUTH_KEY',  'jBUxCC=??:8DkrDBncOIsl<ys%|xZ9:_#q-b{>D9JM#lU*cn`0UQzB*w^jb/4,(b');
define('LOGGED_IN_KEY',    'ANgzP[ML#tFERVXAAk,.3JrlJ!Y,02f,a$mI0|@WbeN5RK[p%U/BDM7Y!YI3(_Gx');
define('NONCE_KEY',        '>_+7%;wI D%r6n}.(|4h^k~xpTBZ<D_iqs_5$;RnmG@dZK$S!O_:}goVtI3)zNat');
define('AUTH_SALT',        'u#Fnl/!4z6b(rG+y(!sn>M6LubB8pxl$O8*;+~xT7-9eQ0;))j{UQ+)3$9,hr-qt');
define('SECURE_AUTH_SALT', ',4+KW42LGH).IH!([inw_&{$].?o&<PKZQ_8]6eJUCzFI:hL]$6hPA32N]~N#/wg');
define('LOGGED_IN_SALT',   'rfU $G,_3!}LR~E54O(n0:(Br nX7E5$=Q.Z?lP?>p}S+wr_aN]qIzG(zvXfiR5j');
define('NONCE_SALT',       'IjgK5|f26wNrHe~Fu]RYYWL=O2/) V*dHT<N7y+K]][)CdlOc>/_-X-PcWR*]A=1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
