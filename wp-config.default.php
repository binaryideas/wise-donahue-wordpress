<?php
/**
 * Default config settings
 *
 * Enter any WordPress config settings that are default to all environments
 * in this file. These can then be overridden in the environment config files.
 * 
 * Please note if you add constants in this file (i.e. define statements) 
 * these cannot be overridden in environment config files.
 * 
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */
  

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'C|J!/{;7}fZP=1[clEVnZG+9P`2uY]2pTSP JO=CUQE1Av4/!p9=WjvtO)6t718u');
define('SECURE_AUTH_KEY',  'jBUxCC=??:8DkrDBncOIsl<ys%|xZ9:_#q-b{>D9JM#lU*cn`0UQzB*w^jb/4,(b');
define('LOGGED_IN_KEY',    'ANgzP[ML#tFERVXAAk,.3JrlJ!Y,02f,a$mI0|@WbeN5RK[p%U/BDM7Y!YI3(_Gx');
define('NONCE_KEY',        '>_+7%;wI D%r6n}.(|4h^k~xpTBZ<D_iqs_5$;RnmG@dZK$S!O_:}goVtI3)zNat');
define('AUTH_SALT',        'u#Fnl/!4z6b(rG+y(!sn>M6LubB8pxl$O8*;+~xT7-9eQ0;))j{UQ+)3$9,hr-qt');
define('SECURE_AUTH_SALT', ',4+KW42LGH).IH!([inw_&{$].?o&<PKZQ_8]6eJUCzFI:hL]$6hPA32N]~N#/wg');
define('LOGGED_IN_SALT',   'rfU $G,_3!}LR~E54O(n0:(Br nX7E5$=Q.Z?lP?>p}S+wr_aN]qIzG(zvXfiR5j');
define('NONCE_SALT',       'IjgK5|f26wNrHe~Fu]RYYWL=O2/) V*dHT<N7y+K]][)CdlOc>/_-X-PcWR*]A=1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wd_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * Increase memory limit. 
 */
define('WP_MEMORY_LIMIT', '64M');